# ISW란 무엇인가?

- IBM Industry Services Workbench는 은행이 디지털 비즈니스 기회를 클라우드 네이티브 솔루션 으로 전환할 수 있도록 지원
- ISW는 다양한 역할을 하는 사람들이 마이크로서비스를 공동으로 설계, 구현 및 테스트할 수 있도록 하는 데 중점
- ISW를 통해 배포된 솔루션은 REST 기반 API를 통해 다양한 채널에서 사용

## ISW 소개

![ISW 구조](images/fsw1.png)

ISW는 다음 그림과 같이 3개의 Components로 구성

### Solution Designer

- Solution Designer는 솔루션 작성자, 즉 비즈니스 분석가와 개발자가 공유 저장소를 통해 원활하게 함께 작업할 수 있도록 하는 도구 세트
- 도메인 주도 설계 원칙에 따라 비즈니스 도메인을 모델링하는 데 사용할 수 있는 코드 없는 설계 도구
- 개발자는 로우 코드 방식으로 도메인이 모델링된 코드를 사용하여 프로젝트를 구현하며, 도메인 모델에 대한 변경 사항은 생성된 코드에 자동으로 반영되므로
  개발자는 변경된 사양에 맞게 설계와 개발이 구분되어 안전하게 개발
- 도메인 사양을 공유하거나 사전 구축된 도메인 사양을 가져와서 새 프로젝트 개발을 빠르게 시작할 수 있는 로컬 마켓플레이스에 대한 액세스를 제공

### Solution Hub

- Solution Hub는 개발된 프로젝트가 자동으로 배포되는 OpenShift 프로젝트와 Solution Designer를 연결
- Solution Hub에서는 IBM Financial Services Workbench 의 연결된 모든 OpenShift 프로젝트 와 배치에 액세스할 수 있습니다 .
- 빌드 및/또는 배포 파이프라인 구성을 관리
- 사전 정의된 파이프라인 작업에는 광범위한 Testing이 포함되어 구현 코드가 도메인 모델 요구 사항을 충족하고 종속성에 보안 문제가 없으며 대상 환경이 구현 기준을 충족하는지 확인

### Solution Envoy

- Solution Envoy를 OpenShift 프로젝트에 배치하여 IBM Financial Services Workbench로 빌드된 프로젝트를 실행, 관리 및 모니터링
- OpenShift에서 실행되며 확장성과 강력하고 안전한 운영을 위한 기반을 마련

![ISW 구성](images/fsw2.png)

## ISW 구성요소

### Solution Designer

![ISW Solution Designer #1](images/fsw3.png)

- Solution Designer는 복잡한 비즈니스 문제에 대한 솔루션을 공동으로 생성하기 위한 도구 세트
- 모든 솔루션은 프로젝트로 표현

#### 프로젝트 유형

Solution Designer는 현재 두 가지 유형의 서비스 프로젝트를 지원하며 두 유형 모두 구현한 단일 마이크로서비스 표현
Solution Designer 의 프로젝트 페이지는 두 유형에 대한 개요를 제공하므로 모든 프로젝트에 빠르게 액세스

- Low-code Project

  - 로우 코드 프로젝트는 비즈니스 분석가 와 개발자 모두에게 최대한의 지원을 제공하며 ISW의 가장 권장되는 개발 방법으로 전체 모델링 프로세스는 도메인 주도 설계 원칙을 따르는 코드 없는 설계 환경에서 수행이후 커밋된 디자인 모델을 처리하여 구현 코드 기반을 생성하며 각 모델링된 요소는 사전 생성된 모든 필수 메소드로 코드 표현된다.
  - 소프트웨어 개발자는 통신 프로토콜이나 보안 문제와 같은 기본적인 사항에 신경 쓰지 않고 바로 시작할 수 있으며 서비스 프로젝트를 로컬 시스템에 복제할 때 이미 모든 것이 준비되어 있으므로 서비스의 비즈니스 논리에만 집중가능
  - 다음 언어를 지원 : Java SpringBoot, TypeScript

- Pro-code Project
  - 프로 코드 프로젝트는 소스 코드를 완전히 제어해야 하고 코드 없는 디자인 및 로우 코드 개발 지원을 사용지 않거나 필요가 없는 상황에 사용
  - 지속적인 배포 및 보안 기능의 이점만 누리고 나머지는 스스로 처리해야 하는 프로젝트
  - 다음 언어를 지원 : Java SpringBoot, TypesScript, JavaScript

![ISW Solution Designer #2](images/fsw4.png)

그림은 개발과 운영을 구분하여 ISW이 설치되고 사용되는 부분을 도식화 한다.

### Solution Hub

![ISW Solution Hub #1](images/fsw5.png)

Solution Hub는 각 k5-project의 환경을 관리하는 기능과, Topic을 관리하는 두가지의 기능을 제공

- Enviroment : 클러스터의 모든 k5 프로젝트 목록을 제공하고 각 프로젝트의 상세 환경을 설정

  - Deployments : 각 배포된 프로젝트들의 인증, DB, 로깅, 메시지, 테스팅들의 환경을 설정
  - Project Configuration : 전체 k5의 디폴드 프로젝트를 관리
  - Topic Bindings : k5-project내에서 Messaging에 사용할 Topic을 관리
  - Pipeline Runs : 사용할 Pipeline의 관리

- Topic Bindings : 전체에서 사용될 Messaging Topic을 관리

### Solution Envoy

![ISW Solution Hub #1](images/fsw6.png)

Solution Envoy는 각 k5-project마다 존재하며 서비스 프로젝트들의 정보 및 Swagger등에 접속가능

- Solutions
  - 배포된 각 솔루션들의 목록을 확인 가능하며 현재 Status도 모니터링 가능, Swagger가 작성된 프로젝트의 경우 Swagger-ui 페이지로 이동되어 배포된 api들을 확인 가능
- Storage
  - 배포된 각 솔루션들에 연결된 Database들의 상태를 확인할 수 있다.
- Messaging
  - 사욛되는 토픽들의 사용 현황을 확인 할 수 있다.
- Infrastructure
  - 현재 사용하는 k5-project에 사용되는 envoy의 정보를 확인할 수 있으며 CLI연결을 도와준다.

## ISW 개발 흐름도

![ISW #1](images/fsw7.png)

ISW를 사용한는 흐름에 대해서 설명한다.
